package de.asideas.images.modules;

import android.app.Application;
import android.net.Uri;

import de.asideas.images.api.ShutterStockService;
import com.google.gson.Gson;
import com.squareup.okhttp.Authenticator;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Credentials;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.Proxy;

import javax.inject.Qualifier;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import retrofit.Endpoint;
import retrofit.Endpoints;
import retrofit.RestAdapter;
import retrofit.client.Client;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;
import timber.log.Timber;

@Module(injects = {}, library = true, complete = false)
public class ProdModule {

    static final int DISK_CACHE_SIZE = 50 * 1024 * 1024; // 50MB
    public static final String SHUTTERSTOCK_URL = "https://api.shutterstock.com/v2/images/";
    public static final String CLIENT_ID = "d630f0c91dcf1055bc7e";
    public static final String CLIENT_SECRET = "ec22dde36ad1a9bf7952ecd533755e94c7748503";
    public static final String OKHTTP_AUTH_QUALIFIER = "okhttp_auth_qualifier";
    public static final String OKHTTP_CACHE_QUALIFIER = "okhttp_basic_qualifier";

    @Provides
    @Singleton
    Endpoint provideShutterStockEndpoint() {
        return Endpoints.newFixedEndpoint(SHUTTERSTOCK_URL);
    }

    @Provides
    @Singleton
    @Named(OKHTTP_AUTH_QUALIFIER)
    OkHttpClient provideOkHttpClientWithAuth(final Application app) {
        return createOkHttpClientWithAuth(app);
    }

    @Provides
    @Singleton
    @Named(OKHTTP_CACHE_QUALIFIER)
    OkHttpClient provideOkHttpClient(final Application app) {
        return createOKHttpClientWithCache(app);
    }

    @Provides
    @Singleton
    Client provideClientWithAuth(final @Named(OKHTTP_AUTH_QUALIFIER) OkHttpClient client) {
        return new OkClient(client);
    }

    @Provides
    @Singleton
    RestAdapter provideRestAdapter(final Client client, final Endpoint endpoint) {
        RestAdapter.Builder builder = new RestAdapter.Builder();
        return new RestAdapter.Builder().setClient(client).setEndpoint(endpoint).setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(new Gson())).build();
    }

    @Provides
    @Singleton
    ShutterStockService provideShutterStockService(RestAdapter restAdapter) {
        return restAdapter.create(ShutterStockService.class);
    }

    @Provides
    @Singleton
    Picasso providePicasso(final Application app, final @Named(OKHTTP_CACHE_QUALIFIER) OkHttpClient client) {
        return new Picasso.Builder(app).downloader(new OkHttpDownloader(client)).listener(new Picasso.Listener() {
            @Override
            public void onImageLoadFailed(final Picasso picasso, final Uri uri,
                                          final Exception e) {
                Timber.e(e, "Failed to load image: %s", uri);
            }
        }).build();
    }

    static OkHttpClient createOkHttpClientWithAuth(final Application app) {
        return new OkHttpClient().setAuthenticator(createOkHttpAuthenticator());
    }

    static OkHttpClient createOKHttpClientWithCache(final Application app) {
        OkHttpClient client = new OkHttpClient();
        File cacheDir = new File(app.getCacheDir(), "http");
        Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);
        client.setCache(cache);
        return client;
    }

    private static Authenticator createOkHttpAuthenticator() {
        return new Authenticator() {
            @Override
            public Request authenticate(Proxy proxy, Response response) throws IOException {
                String credential = Credentials.basic(CLIENT_ID, CLIENT_SECRET);
                return response.request().newBuilder()
                        .header("Authorization", credential)
                        .build();
            }

            @Override
            public Request authenticateProxy(Proxy proxy, Response response) throws IOException {
                return null;
            }
        };
    }

    @Qualifier
    @Documented
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Named {
        String value() default "";
    }
}
