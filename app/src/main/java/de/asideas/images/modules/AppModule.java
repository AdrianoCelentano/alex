package de.asideas.images.modules;

import android.app.Application;

import de.asideas.images.api.ImageSearchTask;
import de.asideas.images.common.AxelApp;
import de.asideas.images.ui.AxelActivity;
import de.asideas.images.ui.AxelFragment;
import de.asideas.images.ui.ImageListAdapter;

import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

@Module(includes = { ProdModule.class },
        injects = { AxelApp.class, AxelActivity.class, AxelFragment.class, ImageSearchTask.class,
        ImageListAdapter.class } )

public final class AppModule {

    private final AxelApp app;

    public AppModule(AxelApp app) {
        this.app = app;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return app;
    }
}
