package de.asideas.images.model;

import com.google.gson.annotations.Expose;

public class Contributor {

    @Expose
    private String id;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }
}