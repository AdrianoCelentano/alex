package de.asideas.images.ui;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import de.asideas.images.model.Datum;
import de.asideas.images.model.Preview;
import com.example.adrian.axel.R;
import de.asideas.images.util.InjectionUtils;
import de.asideas.images.util.PixelDpConverter;
import de.asideas.images.util.SystemUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ImageListAdapter extends RecyclerView.Adapter {

    private static final float IMAGE_MARGIN = 32;
    private static final float IMAGEM_HEIGHT_IN_DP = 320;
    private int imageWidth;
    private int imageHeight;
    private List<Datum> data;
    @Inject
    Picasso picasso;

    public ImageListAdapter(Context context, List<Datum> data) {
        this.data = data;
        setImageDimensions(context);
        InjectionUtils.inject(context, this);
    }

    private void setImageDimensions(Context context) {
        DisplayMetrics displayMetrics = SystemUtils.getDisplayMetrics((Activity) context);
        this.imageWidth = (int) (displayMetrics.widthPixels - PixelDpConverter.convertDpToPixel(IMAGE_MARGIN, context));
        this.imageHeight = (int) PixelDpConverter.convertDpToPixel(IMAGEM_HEIGHT_IN_DP, context);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.image_list_item_image_view)
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_list_item_layout, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Preview currentImage = data.get(position).getAssets().getPreview();
        loadImage((ViewHolder) holder, currentImage);
    }

    private void loadImage(ViewHolder holder, Preview currentImage) {
        ImageView imageView = holder.imageView;
        picasso.load(currentImage.getUrl())
                .resize(imageWidth, imageHeight)
                .centerCrop()
                .into(imageView);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
