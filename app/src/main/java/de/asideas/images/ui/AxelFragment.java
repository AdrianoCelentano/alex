package de.asideas.images.ui;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.example.adrian.axel.R;
import butterknife.InjectView;
import de.asideas.images.api.ImageSearchTask;
import de.asideas.images.common.BaseFragment;
import de.asideas.images.model.ImageSearchResult;

public class AxelFragment extends BaseFragment implements ImageSearchTask.ImageSearchListener {

    private static final String BUNDLE_EXTRA_RECYCLER_VIEW_STATE = "bundle_extra_recycler_view_state";

    @InjectView(R.id.axel_fragment_fragment_recycler_view)
    RecyclerView recyclerView;

    public AxelFragment() {}

    @Override
    protected int layoutToInflate() {
        return R.layout.axel_fragment_layout;
    }

    public static AxelFragment getInstance() {
        return new AxelFragment();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new ImageSearchTask(getActivity(), "axel springer", this).execute();
        setupUI();
    }

    @Override
    protected void initFromBundle(Bundle arguments) {
        super.initFromBundle(arguments);
        Parcelable savedRecyclerLayoutState = arguments.getParcelable(BUNDLE_EXTRA_RECYCLER_VIEW_STATE);
        recyclerView.getLayoutManager().onRestoreInstanceState(savedRecyclerLayoutState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_EXTRA_RECYCLER_VIEW_STATE, recyclerView.getLayoutManager().onSaveInstanceState());
    }

    private void setupUI() {
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onImageSearchResultsLoaded(ImageSearchResult imageSearchResult) {
        ImageListAdapter adapter = new ImageListAdapter(getActivity(), imageSearchResult.getData());
        recyclerView.setAdapter(adapter);
    }
}
