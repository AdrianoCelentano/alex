package de.asideas.images.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import de.asideas.images.common.BaseActivity;
import com.example.adrian.axel.R;


public class AxelActivity extends BaseActivity {

    private static final String AXEL_FRAGMENT_TAG = "axel_fragment_tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setBackgroundDrawable(null);

        FragmentManager fragmentManager = getSupportFragmentManager();
        AxelFragment axelFragment = (AxelFragment) fragmentManager.findFragmentByTag(AXEL_FRAGMENT_TAG);

        if (axelFragment == null) {
            addFragment(R.id.axel_axel_fragment_frame_layout, AxelFragment.getInstance(), AXEL_FRAGMENT_TAG);
        }
    }

    @Override
    protected Intent createIntent(Context context) {
        return new Intent(context, AxelActivity.class);
    }

    @Override
    protected int getContentLayoutId() {
        return R.layout.axel_activity_layout;
    }

}
