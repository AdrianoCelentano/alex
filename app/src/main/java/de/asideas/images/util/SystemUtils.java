package de.asideas.images.util;

import android.app.Activity;
import android.util.DisplayMetrics;

public final class SystemUtils {

    private SystemUtils () {

    }

    public static DisplayMetrics getDisplayMetrics (Activity context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics;
    }
}
