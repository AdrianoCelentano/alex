package de.asideas.images.util;

import android.content.Context;

import de.asideas.images.common.AxelApp;


public final class InjectionUtils {

    private InjectionUtils () {}

    public static void inject (Context context, Object o) {
        ((AxelApp)context.getApplicationContext()).inject(o);
    }
}