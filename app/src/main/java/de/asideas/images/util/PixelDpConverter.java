package de.asideas.images.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

public final class PixelDpConverter {

    private PixelDpConverter () {}

    public static float convertDpToPixel(final float dp, final Context context) {
        if (dp < 0) {
            throw new IllegalArgumentException("dp cannot be negative.");
        }
        else {
            Resources resources = context.getResources();
            DisplayMetrics metrics = resources.getDisplayMetrics();
            float px = dp * (metrics.densityDpi / 160f);
            return px;
        }
    }

    public static float convertPixelsToDp(final float px, final Context context) {
        if (px < 0) {
            throw new IllegalArgumentException("px cannot be negative.");
        }
        else {
            Resources resources = context.getResources();
            DisplayMetrics metrics = resources.getDisplayMetrics();
            float dp = px / (metrics.densityDpi / 160f);
            return dp;
        }
    }
}
