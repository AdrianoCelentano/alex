package de.asideas.images.api;

import retrofit.RequestInterceptor;

public class ApiHeaders implements RequestInterceptor {
    private String authValue;

    public ApiHeaders(String authValue) {
        this.authValue = "Basic " + authValue;
    }

    public void clearAuthValue() {
        authValue = null;
    }

    public void setAuthValue(String authValue) {
        this.authValue = "Basic " + authValue;
    }

    public boolean isLoggedIn() {
        String authValue = this.authValue;
        if (authValue != null) {
            return true;
        }
        else return false;
    }

    @Override public void intercept(RequestFacade request) {
        String authValue = this.authValue;
        if (authValue != null) {
            request.addHeader("Authorization", authValue);
        }
    }
}
