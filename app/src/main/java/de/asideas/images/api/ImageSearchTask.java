package de.asideas.images.api;

import android.content.Context;

import de.asideas.images.common.AbstractAsyncTask;
import de.asideas.images.model.ImageSearchResult;

import javax.inject.Inject;

public class ImageSearchTask extends AbstractAsyncTask<Void, Void, ImageSearchResult> {

    private static final String IMAGE_SIZE = "m";

    @Inject
    ShutterStockService shutterStockService;

    private String searchQuery;
    private ImageSearchListener imageSearchListener;

    public ImageSearchTask(Context context, String searchQuery, ImageSearchListener imageSearchListener) {
        super(context);
        this.searchQuery = searchQuery;
        this.imageSearchListener = imageSearchListener;
    }

    @Override
    protected ImageSearchResult doInBackground(Void... params) {
        return shutterStockService.searchImages(searchQuery, IMAGE_SIZE);
    }

    @Override
    protected void onPostExecute(ImageSearchResult imageSearchResult) {
        super.onPostExecute(imageSearchResult);
        imageSearchListener.onImageSearchResultsLoaded(imageSearchResult);
    }

    public interface ImageSearchListener {
        void onImageSearchResultsLoaded(ImageSearchResult imageSearchResult);
    }
}
