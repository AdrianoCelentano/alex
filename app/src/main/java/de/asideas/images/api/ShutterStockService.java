package de.asideas.images.api;

import de.asideas.images.model.ImageSearchResult;

import retrofit.http.GET;
import retrofit.http.Query;

public interface ShutterStockService {

    @GET("/search")
    ImageSearchResult searchImages(@Query("query") String searchQuery, @Query("size") String imageSize);

}
