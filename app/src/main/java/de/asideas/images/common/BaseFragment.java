package de.asideas.images.common;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.asideas.images.util.InjectionUtils;

import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        InjectionUtils.inject(getActivity(), this);

        Bundle arguments = getArguments();
        if (arguments != null) {
            initFromBundle(arguments);
        }
        if(savedInstanceState != null) {
            initFromBundle(savedInstanceState);
        }
    }

    protected void initFromBundle(Bundle arguments) {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(layoutToInflate(), container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    protected abstract int layoutToInflate();
}
