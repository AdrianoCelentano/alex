package de.asideas.images.common;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import de.asideas.images.util.InjectionUtils;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentLayoutId());
        InjectionUtils.inject(this, this);
        ButterKnife.inject(this);
        if(savedInstanceState != null) {
            initiateFromBundle(savedInstanceState);
        }
        if(getIntent() != null && getIntent().getExtras() != null) {
            initiateFromBundle(getIntent().getExtras());
        }
    }

    protected abstract Intent createIntent(Context context);

    protected abstract int getContentLayoutId();

    protected void initiateFromBundle(Bundle bundle) {

    }

    public void addFragment(@IdRes final int fragmentLayoutId, final BaseFragment fragment, String tag) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(fragmentLayoutId, fragment, tag);
        fragmentTransaction.commit();
    }
}
