package de.asideas.images.common;

import android.content.Context;
import android.os.AsyncTask;

import de.asideas.images.util.InjectionUtils;

public abstract class AbstractAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    public AbstractAsyncTask(final Context context) {
        InjectionUtils.inject(context, this);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
    }
}
