package de.asideas.images.common;

import de.asideas.images.modules.AppModule;

final class Modules {
    static Object[] list(AxelApp app) {
        return new Object[] {
                new AppModule(app)
        };
    }

    private Modules() {
        // No instances.
    }
}