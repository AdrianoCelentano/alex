package de.asideas.images;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import de.asideas.images.util.PixelDpConverter;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;


@RunWith(RobolectricTestRunner.class)
public class AxelTest {

    Context context;

    @Before
    public void setUp() throws Exception {
        context = Robolectric.application;
    }

    @Test
    public void PixeDpConvertertTest() throws IndexOutOfBoundsException {

        float pixel;

        pixel =  PixelDpConverter.convertDpToPixel(20, context);
        assertThat(pixel > 0, is(true));

        pixel = PixelDpConverter.convertDpToPixel(0, context);
        assertThat(pixel == 0, is(true));
    }

    @Test(expected=IllegalArgumentException.class)
    public void testIllegalArgumentException() {
        float pixel = PixelDpConverter.convertDpToPixel(-20, context);
    }
}